#include <iostream>
#include <vector>

extern int a_call();
extern int b_call();

int main()
{
    std::cout << a_call() << std::endl;
    std::cout << b_call() << std::endl;
    return 0;
}
