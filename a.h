#pragma once

template<typename T>
T my_add(T a, T b) {
    return a + b;
}

template<>
inline int my_add(int a, int b) {
    return 10 * a + b;
}